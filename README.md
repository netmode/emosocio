### Ontology Documentation
This repository contains information regarding the specification of the EmoSocio Ontology. The code regarding the documentation of the Ontology is available under the "public" folder, while an interactive documentation of the [EmoSocio Ontology](https://gitlab.com/netmode/emosocio/-/blob/master/emosociolatest.ttl) can be found [here](https://netmode.gitlab.io/emosocio).



### Evaluation Data
This repository also hosts an open anonymized dataset used for the evaluation of the first version of the EmoSocio model. The dataset is available under the "data/EmoSocioV1" folder and is composed of five files in tabular format.
| File | Description |
| ------ | ------ |
| EmoSocioInventoryResponses.csv | contains  153 responses to the EmoSocio Inventory |
| EmoSocioInventoryResponsesRetest.csv | contains  20 responses to the EmoSocio Inventory. These responses have been realized three weeks after the first assesment of the Emosocio Inventory |
| NeoPiResults.csv | contains  21 responses to the Short Form for the [International Personality Item Pool Representation of the NEO PI-R (IPIP-NEO)](http://www.personal.psu.edu/%7Ej5j/IPIP/ipipneo120.htm) |
| PetridesTEI-QueResponses.csv | contains  22 responses to the [Trait Emotional Intelligence Questionnaire (TEI-Que)](http://psychometriclab.com/obtaining-the-teique/)  |
| Schutte InventoryResponses.csv | contains  21 responses to the [ Schutte Self-Report Emotional Intelligence Test (SSEIT)](https://www.statisticssolutions.com/schutte-self-report-emotional-intelligence-test-sseit/) |

This repository also hosts an open anonymized dataset used for the evaluation of the second version of the EmoSocio model. The dataset is available under the "data/EmoSocioV2" folder and is composed of 2 files in tabular format and 1 file in json format.
| File | Description |
| ------ | ------ |
| MemberRawResponsesEmo.csv | contains  537 responses to the Emo part of the EmoSocio Inventory |
| MemberRawResponsesSocio.csv | contains  493 responses to the Socio part of the EmoSocio Inventory |
| emosociograms.json | contains  17 emosociograms in networkx json format that represent the social dynamics between the group members that have used the [EmoSociograms psychometric tool](https://gitlab.com/netmode/emosociograms) |

### Contact

For any request for detailed information or clarifications related to the dataset and the ontology, you may contact:
- Eleni Fotopoulou - efotopoulou (at) netmode (dot) ntua (dot) gr
- Anastasios Zafeiropoulos - tzafeir (at) cn (dot) ntua (dot) gr

### Cite

To cite this work, please use:

_Eleni Fotopoulou, Anastasios Zafeiropoulos, Symeon Papavassiliou_, **EmoSocio: an open access Sociometry-enriched Emotional Intelligence model**, Current Research in Behavioral Sciences, 2021, 100015, ISSN 2666-5182, https://doi.org/10.1016/j.crbeha.2021.100015.

_E. Fotopoulou, A. Zafeiropoulos, È. L. Cassà, I. M. Guiu and S. Papavassiliou_, **Collective Emotional Intelligence and Group Dynamics Interplay: Can It Be Tangible and Measurable?**, in IEEE Access, vol. 10, pp. 951-967, 2022, doi: [10.1109/ACCESS.2021.3137051](https://ieeexplore.ieee.org/document/9656748).


### License
<img src="https://licensebuttons.net/l/by-nc-sa/3.0/88x31.png" alt="CC BY-NC-SA 4.0">

(c) 2020 by Eleni Fotopoulou, Anastasios Zafeiropoulos (National Technical University of Athens)

This work is licensed under a [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/) Public License.

